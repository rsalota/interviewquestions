package arrays.medium;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MedianOf2ArraysTest {
	MedianOf2Arrays m ;
	
	@Before
	public void setup(){
		m = new MedianOf2Arrays();
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNullArrays(){
		m.median(null, null);
	}

	@Test
	public void testOnlyRightArrayWith1Number(){
		int [] array1 = {1};
		double value =m.median(array1, null);
		Assert.assertTrue(value == 1);
	}
	
	@Test
	public void testOnlyLeftArrayWith1Number(){
		int [] array1 = {1};
		double value =m.median(null, array1);
		Assert.assertTrue(value == 1);
	}
	
	@Test
	public void testOnlyBothArrayWith1NumberEach(){
		int [] array1 = {1};
		int [] array2 = {2};
		double value =m.median(array2, array1);
		Assert.assertTrue(value == 1.5);
	}
	
	@Test
	public void testOnlyBothArrayWithMultipleNumberEachEven(){
		int [] array1 = {1,2,3,4};
		int [] array2 = {5,6,7,9};
		double value =m.median(array2, array1);
		Assert.assertTrue(value == 4.5);
	}
	
	@Test
	public void testOnlyBothArrayWithMultipleNumberEachOdd(){
		int [] array1 = {1,2,3};
		int [] array2 = {5,6,7,9};
		double value =m.median(array2, array1);
		Assert.assertTrue(value == 5);
	}
	
	@Test
	public void testOneEmptyArray(){
		int [] array1 = {};
		int [] array2 = {5,6,7,9};
		double value =m.median(array2, array1);
		Assert.assertTrue(value == 6.5);
	}

}
