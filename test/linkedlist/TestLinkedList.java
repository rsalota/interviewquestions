package linkedlist;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import linkedlist.medium.SumOfNumbers;

public class TestLinkedList {
	SumOfNumbers s = new SumOfNumbers();

	@Before
	public void setUp(){
		s = new SumOfNumbers();
	}
	
	@Test
	public void testNullListNode(){
		ListNode l =s.addTwoNumbers(null, null);
		Assert.assertTrue(l==null);
	}
	
	@Test
	public void testFirstNodeIsNullListNode(){
		ListNode l =s.addTwoNumbers(null, new ListNode(10));
		Assert.assertTrue(l.value==10);
	}
	
	@Test
	public void testSecondListIsNullListNode(){
		ListNode l =s.addTwoNumbers(new ListNode(10),null);
		Assert.assertTrue(l.value==10);
	}
	
	@Test
	public void testSimpleAddListNode(){
		ListNode first = new ListNode(1);
		ListNode second = new ListNode(2);
		ListNode l =s.addTwoNumbers(first,second);
		Assert.assertTrue(l.value==3);
	}
	
	@Test
	public void testSimpleAddWithDanglingCarryListNode(){
		ListNode first = new ListNode(3);
		ListNode second = new ListNode(9);
		ListNode l =s.addTwoNumbers(first,second);
		Assert.assertTrue(l.value==2);
		Assert.assertTrue(l.next.value==1);
	}
	
	@Test
	public void testSimpleEquiSizedListNodes(){
		ListNode first = new ListNode(1);
		ListNode firstNext = new ListNode(1);
		
		ListNode second = new ListNode(2);
		ListNode secondNext = new ListNode(4);
		
		first.next = firstNext;
		second.next = secondNext;
		
		ListNode l =s.addTwoNumbers(first,second);
		Assert.assertTrue(l.value==3);
		Assert.assertTrue(l.next.value==5);
	}
	
	@Test
	public void testSimpleNonEquiSizedListNodes(){
		ListNode first = new ListNode(1);
		ListNode firstNext = new ListNode(1);
		
		ListNode second = new ListNode(2);

		
		first.next = firstNext;
		
		
		ListNode l =s.addTwoNumbers(first,second);
		Assert.assertTrue(l.value==3);
		Assert.assertTrue(l.next.value==1);
		
	}

	@Test
	public void testSimpleNonEquiSizedWhichCausesCarryListNodes(){
		ListNode first = new ListNode(1);
		ListNode firstNext = new ListNode(1);
		ListNode second = new ListNode(9);
		first.next = firstNext;
		ListNode l =s.addTwoNumbers(first,second);
		Assert.assertTrue(l.value==0);
		Assert.assertTrue(l.next.value==2);
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNegativeElementInOneNode(){
		ListNode first = new ListNode(1);
		ListNode firstNext = new ListNode(1);
		ListNode second = new ListNode(-9);
		first.next = firstNext;
		ListNode l =s.addTwoNumbers(first,second);
	}
	
	
}
