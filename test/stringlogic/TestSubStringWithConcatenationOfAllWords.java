package stringlogic;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestSubStringWithConcatenationOfAllWords {
	SubStringWithConcatenationOfAllWords subStringWithConcatenationOfAllWords;
	
	@Before
	public void setUp(){
		subStringWithConcatenationOfAllWords = new SubStringWithConcatenationOfAllWords();
	}
	
	@Test
	public void basicFooBarTest(){
		String str = "foobarbarfoo";
		String[] words={"foo","bar"};
		Integer[] beginIndices= subStringWithConcatenationOfAllWords.indices(str, words);
		Assert.assertTrue(beginIndices[0]==0);
		Assert.assertTrue(beginIndices[1]==6);
	}
	
	@Test
	public void testBasicFooBarWhenAsymetric(){
		String str = "foobarxbarfooxxx";
		String[] words={"foo","bar"};
		Integer[] beginIndices= subStringWithConcatenationOfAllWords.indices(str, words);
		Assert.assertTrue(beginIndices[0]==0);
		Assert.assertTrue(beginIndices[1]==7);
	}

	@Test
	public void testBasicFooBarWhenMoreWords(){
		String str = "foobarbarfooxxx";
		String[] words={"foo","bar","news"};
		Integer[] beginIndices= subStringWithConcatenationOfAllWords.indices(str, words);
		Assert.assertTrue(beginIndices.length==0);
	}

}
