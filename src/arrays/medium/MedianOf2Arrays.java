package arrays.medium;

public class MedianOf2Arrays {
	public double median(int [] sorted1 , int [] sorted2 ){
		
		if (sorted1 == null && sorted2 == null){
			throw new IllegalArgumentException();
		}
		
		if (sorted1 == null|| sorted1.length == 0 ){
			return calculateSingleArrayMedian(sorted2);
		}
		
		if (sorted2 == null|| sorted2.length == 0 ){
			return calculateSingleArrayMedian(sorted1);
		}
		
		int sorted1Index=0;
		int sorted2Index=0;
		
		int [] mergedArray= new int[sorted1.length + sorted2.length];
		int index=0;
		while(sorted1.length > sorted1Index || sorted2.length > sorted2Index){
			if (sorted1.length > sorted1Index && sorted2.length > sorted2Index){
				if (sorted1[sorted1Index] >= sorted2[sorted2Index] ){
					mergedArray[index] = sorted2[sorted2Index];
					sorted2Index++;
				}else{
					mergedArray[index] = sorted1[sorted1Index];
					sorted1Index++;
				}
			}else if(sorted1.length > sorted1Index){
				mergedArray[index] = sorted1[sorted1Index];
				sorted1Index++;
			}else{
				mergedArray[index] = sorted2[sorted2Index];
				sorted2Index++;
			}
			index++;
		}
		
		return calculateSingleArrayMedian(mergedArray);
	}
	
	private double calculateSingleArrayMedian(int [] sorted){
		double median=0;
		if (sorted.length%2 == 0){
			int mid = sorted.length/2;
			int midMinus1 = mid-1;
			median=(sorted[mid] + sorted[midMinus1])/2.0;
		}else{
			int mid = sorted.length/2;
			median = sorted[mid];
		}
		return median;
	}
}
