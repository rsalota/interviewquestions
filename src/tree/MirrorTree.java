package tree;

import java.util.Queue;
import java.util.concurrent.LinkedTransferQueue;

public class MirrorTree {
	
	public static void main(String args[]){
		Node root = new Node(1);
		
		Node right = new Node(2);
		Node left = new Node(3);
		
		root.right=right;
		root.left = left;
		
		System.out.println("Before ");
		Node.print(root);
		makeMirrorTree(root);
		System.out.println("After ");
		Node.print(root);
	}
	
	public static void makeMirrorTree(Node root){
		if (root == null || (root.right == null && root.left==null)){
			return;
		}
		Queue<Node> queue = new LinkedTransferQueue<Node>();
		queue.add(root);
		while(!queue.isEmpty()){
			Node newRoot = queue.remove();
			Node tmp = newRoot.left;
			newRoot.left =newRoot.right;
			newRoot.right= tmp;
			if (newRoot.right !=null){
				queue.add(newRoot.right);
			}
			if (newRoot.left !=null){
				queue.add(newRoot.left);
			}
		}
	}
}
