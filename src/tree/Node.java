package tree;

public class Node {
	public int value;
	public Node right;
	public Node left;
	
	public Node(int value){
		this.value = value;
	}
		
	public static void print(Node root){
		System.out.println(root.value);
		if (root.right != null ){
			print(root.right);
		}
		if (root.left!=null){
			print(root.left);
		}
	}
}
