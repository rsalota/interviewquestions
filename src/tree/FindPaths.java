package tree;

import java.util.LinkedList;
import java.util.List;

public class FindPaths {
	
	public static void main (String [] args){
		Node root = new Node(-10);
		Node right = new Node(11);
		Node left = new Node(-11);
		root.right=right;
		root.left = left;
		
		 List<List<Node>> nodesOfNodes = findPaths(-21,root);
		 int count =1;
		for(List<Node> list : nodesOfNodes){
			System.out.println("List " + count);
			for (Node n : list){
				System.out.print(n.value + " ");
			}
			System.out.println("\nend " + count);
			count++;
		}
	}
	
	public static List<List<Node>> findPaths(int remaining, Node root){
		List<List<Node>> newNodesOfNodesList = new LinkedList<List<Node>>();
		if (root == null){
			throw new IllegalArgumentException();
		}
		int newRemaining = remaining - root.value;
		if (isLeafNode(root)){
			if (newRemaining == 0){
				List<Node> path = new LinkedList<>();
				path.add(root);
				newNodesOfNodesList.add(path);
			}
		}else{
			if (root.right != null){
				List<List<Node>> rightSolution = findPaths(newRemaining, root.right);
				for(List<Node> list : rightSolution){
					list.add(root);
					newNodesOfNodesList.add(list);
				}
			}
			if (root.left != null){
				List<List<Node>> leftSolution = findPaths(newRemaining, root.left);
				for(List<Node> list : leftSolution){
					list.add(root);
					newNodesOfNodesList.add(list);
				}
			}
		}
		return newNodesOfNodesList;
	}

	private static boolean isLeafNode(Node root) {
		if (root.left == null && root.right ==null){
			return true;
		}
		return false;
	}

}
