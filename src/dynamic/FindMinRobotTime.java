package dynamic;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class FindMinRobotTime {
	
	public static class Task implements Comparable<Task>{
		int cost;
		Integer index;
		
		public Task(int cost, int index){
			this.cost=cost;
			this.index = index;
		}
		public String toString(){
			return  "_"+index +"_";
		}
		@Override
		public int compareTo(Task o) {
			return index.compareTo(o.index);
		}
	
	}
	
	public static void main(String args[]) throws UnsupportedEncodingException, NoSuchAlgorithmException{
		int tasks[]={1,1,1,1,1,100};
		System.out.println(tasks.length);
		int roboCost = 4;
		System.out.println(findFastestTime(tasks, roboCost));
	}
	
	public static int findFastestTime(int tasks[], int roboCost) throws UnsupportedEncodingException, NoSuchAlgorithmException{
		Set<Task> taskers = new HashSet<>(); 
		for (int i=0;i<tasks.length;i++){
			taskers.add( new Task(tasks[i], i));
		}
		Map<String,Integer> memonization = new HashMap<String,Integer>();
		int value =  findFastestTimeByTask(taskers, roboCost,memonization);
		return value;
	}
	
	private static int findFastestTimeByTask(Set<Task> taskers, int roboCost,
			Map<String, Integer> memonization) throws UnsupportedEncodingException, NoSuchAlgorithmException {
		if (taskers.size() == 1 ){
			return taskers.iterator().next().cost;
		}
		
		if (memonization.containsKey(signature(taskers))){
			return memonization.get(signature(taskers));
		}

		int minTime = Integer.MAX_VALUE;
		for(Task t:taskers){
			//Make a copy of tasks and remove the one we are evaluating.
			Set<Task> tasks = new TreeSet<>();
			tasks.addAll(taskers);
			tasks.remove(t);
			int timeExcludingTaskT = findFastestTimeByTask(tasks, roboCost, memonization);
			memonization.put(signature(tasks), timeExcludingTaskT);
			int parallelExecution = Math.max(timeExcludingTaskT,t.cost) + roboCost ;
			if (minTime > parallelExecution ){
				minTime = parallelExecution;
			}
			int serialExecution = timeExcludingTaskT + t.cost;
			if (minTime > serialExecution){
				minTime = serialExecution; 
			}
		}
		return minTime;
	}
	
	public static String signature(Set<Task> tasks) throws UnsupportedEncodingException, NoSuchAlgorithmException{
		StringBuffer buffer = new StringBuffer();
		for(Task t:tasks){
			buffer.append(t.toString());
		}
		return buffer.toString();
	}


}
