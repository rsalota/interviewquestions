package linkedlist.medium;

import linkedlist.ListNode;

public class SumOfNumbers {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        
        if (l1 == null && l2 == null){
            return null;
        }
        
        if (l1 == null){
            return l2;
        }
        
        if (l2 == null){
            return l1;
        }
        
        int carry=0;
        ListNode cL1=l1;
        ListNode cL2=l2;
        ListNode sumListNodeTail=null;
        ListNode sumListNodeHead=null;
        while(cL1 != null || cL2 != null){
            
            int cL1Value = cL1 !=null ? cL1.value : 0;
            int cL2Value = cL2 !=null ? cL2.value : 0;
            if (cL1Value < 0|| cL2Value < 0 | cL1Value > 10 || cL2Value > 10){
            	throw new IllegalArgumentException("values cannot be greater than 10 ");
            }
            int sum = cL1Value  + cL2Value + carry;
            int newSum = 0;
            if (sum >= 10){
                carry = sum / 10;
                newSum = sum % 10;
            }else{
                newSum = sum;
                carry=0;
            }
            if (sumListNodeHead == null){
                sumListNodeHead = new ListNode(newSum);
                sumListNodeTail = sumListNodeHead;
            }else{
                ListNode n = new ListNode(sum);
                sumListNodeTail.next = n;
                sumListNodeTail = n;
            }
            
            cL1=cL1 != null ? cL1.next : null;
            cL2=cL2 != null ? cL2.next : null;
        }
        
        if (carry > 0){
            ListNode n = new ListNode(carry);
            sumListNodeTail.next = n;
            sumListNodeTail = n;
        }
        return sumListNodeHead;
    }
    
    
}