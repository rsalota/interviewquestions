package stringlogic;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SubStringWithConcatenationOfAllWords {
	class SubstringWithIndex {
		public SubstringWithIndex(String subString2, int startIndex) {
			this.subString = subString2;
			this.fromIndex = startIndex;
		}

		String subString;
		int fromIndex;
	}

	/**
	 * "foobarbarfoo"
	 * 
	 * foo
	 * bar
	 *  
	 * @param str
	 * @param words
	 * @return
	 */
	public Integer[] indices(String str, String[] words) {
		List<Integer> list = new LinkedList<>();
		if (str == null) {
			throw new IllegalArgumentException();
		}
		if (words == null || words.length == 0) {
			throw new IllegalArgumentException();
		}
		int size = words[0].length() * words.length;
		Iterator<SubstringWithIndex> subStringIt = new SubstringIterator(size, str);
		while (subStringIt.hasNext()) {
			SubstringWithIndex substringWithIndex = subStringIt.next();
			if (canBeMadeFromDictonary(substringWithIndex.subString, words)) {
				list.add(substringWithIndex.fromIndex);
			}
		}
		return list.toArray(new Integer[list.size()]);

	}

	private boolean canBeMadeFromDictonary(String subString, String[] words) {
		Map<String,Boolean> dictionary = new HashMap<>();
		int length = words[0].length();
		for(String w:words){
			dictionary.put(w,false);
		}
		 String [] tokenized = tokenize(subString,length);
		 for (String t:tokenized){
			 if (!dictionary.containsKey(t)){
				 return false;
			 }else{
				 dictionary.put(t, true);
			 }
		 }
		 
		 for(Map.Entry<String, Boolean> entry:dictionary.entrySet()){
			 if (entry.getValue() == false){
				 return false;
			 }
		 }
		 
		return true;
	}

	private String[] tokenize(String subString, int length) {
		String[]tokens = new String[subString.length()/length];
		for(int i=0;i*length<subString.length();i++){
			String token = subString.substring(i*length,i*length + length);
			tokens[i] =token;
		}
		return tokens;
	}

	/**
	 * -- abcdefghijklmnop 
	 * 
	 * @author rsalota
	 *
	 */
	class SubstringIterator implements Iterator<SubstringWithIndex> {
		int startIndex = 0;
		int size = 0;
		String str;

		public SubstringIterator(int size, String str) {
			this.size = size;
			this.str = str;
		}

		@Override
		public boolean hasNext() {
			if (startIndex + size > str.length()) {
				return false;
			}
			return true;
		}

		@Override
		public SubstringWithIndex next() {
			String subString = str.substring(startIndex, startIndex + size);
			SubstringWithIndex substringWithIndex = new SubstringWithIndex(subString, startIndex);
			startIndex++;
			return substringWithIndex;
		}
	}
}
