import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PalindromeFinder {
	
	public static void main(String args[]){
		PalindromeFinder p = new PalindromeFinder();
		String cardStrings[] = {"abc","cba","awq","def","fed"};
		int values[] = {100,100,1000,1400,1400};
		int value = p.calculatePalindrome(cardStrings, values);
		System.out.println(value);
	}

	
	public int calculatePalindrome(String[] cardStrings,int [] values){
		if (cardStrings==null || cardStrings.length == 0|| values == null || values.length==0){
			throw new IllegalArgumentException();
		}
		if (cardStrings.length != values.length){
			throw new IllegalArgumentException();
		}
		Card[] cards = convertToCards(cardStrings,values);
		List<Pair> cardPalindromes = findPairPalindromes(cards);
		int total = 0;
		for(Pair p : cardPalindromes){
			if (p.left!=null){
				total+=p.left.value;
			}
			if (p.right!=null){
				total+=p.right.value;
			}
		}
		return total;
	}
	
	private List<Pair> findPairPalindromes(Card[] cards) {
		
		Map<String,List<Pair>> pairs= new HashMap<>();
		for(Card c:cards){
			List<Pair> p= pairs.get(c.card);
			if (pairs.containsKey(c.card)){
				manageLeft(pairs.get(c.card),c);
			}else if(pairs.containsKey(pairs.get(reverse(c.card)))){
				manageRight(pairs.get(reverse(c.card)),c);
			}else{
				List<Pair> newPairs = new LinkedList<>();
				Pair pair = new Pair();
				pair.right =c;
				newPairs.add(pair);
				pairs.put(reverse(c.card), newPairs);
				
			}
		}
		
		List<Pair> onlyPairs= new LinkedList<>();
		Card maxEmptyCard = null;
		for(String key : pairs.keySet()){
			List<Pair> keyPairs = pairs.get(key);
			for (Pair p:keyPairs){
				if (p.left!=null && p.right!=null){
					onlyPairs.add(p);
				}else{
					Card c =  p.left== null ? p.right:p.left; 
					if (isPalindrome(c)){
						if (maxEmptyCard == null){
							maxEmptyCard = c;
						}else{
							if (maxEmptyCard.value < c.value){
								maxEmptyCard = c;
							}
						}
					}
				}
			}
		}
		
		if (maxEmptyCard != null){
			Pair p = new Pair();
			p.right = maxEmptyCard;
			onlyPairs.add(p);
		}
		
		return onlyPairs;
	}


	private boolean isPalindrome(Card c) {
		char[] values = c.card.toCharArray();
		for (int i=0;i<values.length;i++){
			if (values[i]== values[values.length-i-1]){
				continue;
			}else{
				return false;
			}
		}
		return true;
	}


	private void manageRight(List<Pair> pairs, Card c) {
		for (Pair p:pairs){
			if (p.right == null){
				p.right = c;
				return;
			}else if (p.right.value < c.value){
				Card tmp = p.left;
				p.right = c;
				c = tmp;
			}
		}
		
		Pair pNew = new Pair();
		pNew.right = c;
		pairs.add(pNew);
	}
	
	private void manageLeft(List<Pair> pairs, Card c) {
		for (Pair p:pairs){
			if (p.left == null){
				p.left = c;
				return;
			}else if (p.left.value < c.value){
				Card tmp = p.left;
				p.left = c;
				c = tmp;
			}
		}
		
		Pair pNew = new Pair();
		pNew.left = c;
		pairs.add(pNew);
	}

	private String reverse(String card) {
		char[] toReverse = card.toCharArray();
		for (int i=0;i<toReverse.length/2;i++){
			char tmp = toReverse[i];
			toReverse[i]=toReverse[toReverse.length-i-1];
			toReverse[toReverse.length-i-1]=tmp;
		}
		return new String(toReverse);
	}

	private Card[] convertToCards(String [] cardStrings, int [] values){
		Card[] cards = new Card[cardStrings.length];
		for (int i=0;i<cardStrings.length;i++)
		{
			Card card = new Card();
			card.card = cardStrings[i];
			card.value = values[i];
			cards[i]=card;
		}
		return cards;
	}

}
